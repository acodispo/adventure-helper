-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE TABLE `expedition` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `xp_treasure` int(7) NOT NULL DEFAULT 0,
  `xp_monster` int(7) NOT NULL DEFAULT 0,
  `xp_other` int(7) NOT NULL DEFAULT 0,
  `dungeon_level` int(3) NOT NULL DEFAULT 1,
  `average_party_level` int(4) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `expedition_participants` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `expedition_id` int(10) NOT NULL,
  `pc_id` int(10) NOT NULL,
  `pc_class` int(11) NOT NULL,
  `pc_level` int(4) NOT NULL,
  `share` decimal(3,2) NOT NULL DEFAULT 1.00,
  `xp_mod` decimal(3,2) NOT NULL,
  `xp_awarded` int(7) NOT NULL DEFAULT 0,
  `note` text NOT NULL,
  `ts` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `expedition_id` (`expedition_id`),
  KEY `pc_id` (`pc_id`),
  CONSTRAINT `expedition_participants_ibfk_1` FOREIGN KEY (`expedition_id`) REFERENCES `expedition` (`id`),
  CONSTRAINT `expedition_participants_ibfk_2` FOREIGN KEY (`pc_id`) REFERENCES `pcs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pcs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `ts` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `pc_class` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pc_id` int(10) NOT NULL,
  `class_id` int(11) NOT NULL,
  `xp_mod` decimal(3,2) NOT NULL DEFAULT 1.00,
  `ts` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `pcid` (`pc_id`),
  CONSTRAINT `pc_class_ibfk_1` FOREIGN KEY (`pc_id`) REFERENCES `pcs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `rules_classes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `rules_levels` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `class_id` int(10) NOT NULL,
  `level` int(2) NOT NULL,
  `xp_required` int(2) NOT NULL,
  `title` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2020-10-25 16:24:33
