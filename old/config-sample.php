<?php
$config = [
    'hexdescribe' => 'curl -s https://campaignwiki.org/hex-describe/nomap/markdown --data "url=https://werdnaworld.neocities.org/table.txt" --data-urlencode "input=[table name here]"',
    'db' => [
        'driver' => 'mysql',
        'hostname' => 'localhost',
        'port' => 3306,
        'database' => 'adventure_helper',
        'charset' => 'utf8mb4',
        'username' => 'homestead',
        'password' => 'secret'
    ],
    'env' => 'dev',
];
