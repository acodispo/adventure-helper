<?php
require __DIR__ . '/vendor/autoload.php';

/*
 * Next step is to work out what happens if you have two calls to same subtable
 */
$dice_re = 'qr/^(save )?(?:(\d+)d(\d+)(?:x(\d+))?(?:([+-]\d+))?|(\d+))(?: as (.+))?$/';

$abu = [
    ";tab",
    "1,a",
    "2,b"
];

$goal = [
    "tab" => [
        "total" => 3,
        "lines" => [
            [
                "count" => 1,
                "text" => 'a'
            ],
            [
                "count" => 2,
                "text" => 'b'
            ]
        ]
    ]
];

$data = file(__DIR__.'/public/input.txt');


$request = 'test';
$request = 'Weather';
$request = 'human name';
$request = 'lost';
$request = 'hills';

$parsedData = parse_table($data);

/* dump($parsedData); */
echo pick($request,$parsedData);

function parse_table($text) {
    $words = "[^\[\]\n]*";
    foreach ($text as $line) {
        if (preg_match('/^;([^#\r\n]+)/',$line,$m)) {
            // this is the start of a table
            $key = $m[1];
            $data[$key] = [];
            $data[$key]['total'] = 0;
        } elseif (isset($key) && preg_match('/^(\d+),(.*)/',$line,$m)) {
            // this is a definition in a table
            $data[$key]['total'] += $m[1];
            $h['count'] = $m[1];
            $text = preg_replace('/\*\*(.*?)\*\*/','<strong>$1<\/strong>',$m[2]);
            $h['text'] = preg_replace('/\*(.*?)\*/','<em>$1<\/em>',$text);
            $data[$key]['lines'][] = $h;

/*
            # [foo as bar]
            for my $alias ($h{text} =~ /\[$words as ($words)\]/g) {
                $aliases{$alias} = 1;
            }
            # [foo [baz] quux as bar] (one level of indirection allowed
            for my $alias ($h{text} =~ /\[$words\[$words\]$words as ($words)\]/g) {
                $aliases{$alias} = 1;
            }
            */

        } else {
            // don't know what kind of line this is
            //dump("not something we're parsing");
        }
    }

    // check tables
    $words = "[^\[\]\n]*";
    foreach ($data as $table => $content) {
        foreach ($content['lines'] as $key => $line) {
            if (preg_match_all("/\[($words)\]/",$line['text'],$m)) {
                foreach ($m[1] as $sub) {
                    if (!empty($data[$sub])) {
                        $data[$table]['lines'][$key]['subtable'][] = $sub;
                    }
                }
            }
        }
    }
    /*
    # check tables
    for my $table (keys %$data) {
        for my $line (@{$data->{$table}->{lines}}) {
            for my $subtable ($line->{text} =~ /\[($words)\]/g) {
                next if index($subtable, '|') != -1;
                next if $subtable =~ /$dice_re/;
                next if $subtable =~ /^redirect https?:/;
                next if $subtable =~ /^names for (.*)/ and $data->{"name for $1"};
                next if $subtable =~ /^(?:capitalize|titlecase|highlightcase|normalize-elvish) (.*)/ and $data->{$1};
                next if $subtable =~ /^adjacent hex$/; # experimental
                next if $subtable =~ /^same (.*)/ and ($data->{$1} or $aliases{$1} or $1 eq 'adjacent hex');
                next if $subtable =~ /^(?:here|nearby|other|later|with|and|save|store) (.+?)( as (.+))?$/ and $data->{$1};
                $subtable = $1 if $subtable =~ /^(.+) as (.+)/;
                # $log->error("Error in table $table: subtable $subtable is missing")
                #   unless $data->{$subtable};
            }
        }
    }
    */
    return $data;
}

function pick($request,$data) {
    dump($data[$request]);
    $lines = $data[$request]['lines'];
    $pick = pick_text($data[$request]['total'],$lines);

    if (empty($lines[$pick[0]]['subtable'])) {
        $text = $pick[1];
    } else {
        // subtables!
        $text = $pick[1];
        foreach ($lines[$pick[0]]['subtable'] as $key => $subtable) {
            /* dump($key,$subtable); */
            /* dump($data[$key]); */
            /* $subpick = pick_text($data[$subtable]['total'],$data[$subtable]['lines']); */
            $subpick = pick($subtable,$data);
            /* dump($subtable); */
            /* dump($subpick); */
            $text = preg_replace('/\['.$subtable.'\]/', $subpick, $text);
            /* dump($text); */
        }
    }
    return $text;
}

function pick_text($total,$lines) {
    $roll = random_int(1,$total);
    $i = 0;
    foreach ($lines as $key => $line) {
        $i+= $line['count'];
        if ($i>=$roll) {
            return [$key,$line['text']];
        }
    }
    return 'picked nothing';
}

exit();
