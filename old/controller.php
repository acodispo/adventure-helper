<?php
// what are we doing?
if (!isset($action)) {
   if (isset($_POST['action'])) {
      $action = $_POST['action'];
   }
   elseif (isset($_GET['path'])) {
      $action = $_GET['path'];
   } else {
      $action = 'home';
   }
}
if ($action == 'admin') {
    if (isset($_GET['mode'])) {
        if ($_GET['mode']=='edit' && isset($_GET['type'])) {
            $action = 'edit';
        }
        elseif ($_GET['mode']=='export' && isset($_GET['type'])) {
           $action = 'export';
        }
    }
}

require __DIR__ . '/'.$action.'.php';

$view = $view ?? true;
if ($view) {
   require __DIR__ . '/view.php';
}
