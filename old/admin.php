<?php
require_once __DIR__ . '/bootstrap.php';
$v['title'] = "admin";
$pdo = db();
if (isset($_POST['expedition'])) {

    $expedition = $_POST['expedition'];
    $totalPartyLevels = 0;

    $expedition['xp_treasure'] = ($expedition['xp_treasure']<0) ? 0 : $expedition['xp_treasure'];
    $expedition['xp_monster'] = ($expedition['xp_monster']<0) ? 0 : $expedition['xp_monster'];
    $expedition['xp_other'] = ($expedition['xp_other']<0) ? 0 : $expedition['xp_other'];

    // how much total xp for this expedition?
    $expedition['xp_total'] = $expedition['xp_treasure'] + $expedition['xp_monster'] + $expedition['xp_other'];
    $numberOfPCs = count($expedition['primary']);
    if (!isset($expedition['retainer'])) {
        $expedition['retainer'] = [];
    };
    $numberOfRetainers = count($expedition['retainer']);
    $partySize = $numberOfPCs + $numberOfRetainers;
    $totalShares = $numberOfRetainers + ($numberOfPCs * 2);

    $partyIDs = array_merge($expedition['primary'], $expedition['retainer']);

    $party = getMultiplePCwithClassLevel(implode(", ", $partyIDs));

    foreach ($party as $pc) {
        $note = [];
        foreach ($pc['class'] as $class) {
            $note[] = $class['class'] . " " . $class['level']['current']['level'];
            $totalPartyLevels += $class['level']['current']['level'];
        }
        $party[$pc['id']]['note'] = implode(" / ", $note);
    }

    // reduce XP earned for higher level parties on upper depths
    $averagePartyLevel = round($totalPartyLevels/$partySize);
    $divisor = $expedition['dungeon_level'] / $averagePartyLevel;
    $divisor = ($divisor < 1) ? $divisor : 1;

    $adjustedTotalXP = $expedition['xp_total'] * $divisor;

    $expedition['share'] = ceil($adjustedTotalXP / $totalShares);
    $expedition['averagePartyLevel'] = $averagePartyLevel;

    foreach ($party as $pc) {
        if (in_array($pc['id'], $expedition['primary'])) {
            // this is a primary
            $xpEarned = $expedition['share'] * 2;
            $party[$pc['id']]['share'] = 1;
        } elseif (in_array($pc['id'], $expedition['retainer'])) {
            // this is a retainer
            $xpEarned = $expedition['share'];
            $party[$pc['id']]['share'] = .5;
        }
        $perClassEarned = $xpEarned / count($pc['class']);
        foreach ($pc['class'] as $id => $class) {
            $party[$pc['id']]['class'][$id]['xp_earned'] = ceil($perClassEarned * $class['xp_mod']);
        }
    }

    $expedition['party'] = $party;

    // create a new expedition
    $makeexpedition = $pdo->fetchAffected(
        'INSERT INTO expedition (date, name, xp_treasure, xp_monster, xp_other, dungeon_level, average_party_level) VALUES(?, ?, ?, ?, ?, ?, ?)',
        [
            $expedition['date'],
            $expedition['name'],
            $expedition['xp_treasure'] ?: 0,
            $expedition['xp_monster'] ?: 0,
            $expedition['xp_other'] ?: 0,
            $expedition['dungeon_level'] ?: 1,
            $expedition['averagePartyLevel']
        ]
    );

    $expedition_id = $pdo->lastInsertId();

    if ($makeexpedition != 1) {
        $v['msg'][] = "<strong>Error:</strong> Made more or less than one expedition.";
    } else {
        $stm = 'INSERT INTO expedition_participants (expedition_id,pc_id,pc_class,pc_level,share,xp_mod,xp_awarded,note) VALUES';
        foreach ($expedition['party'] as $pc) {
            foreach ($pc['class'] as $class) {
                $values =  [
                    $expedition_id,
                    $pc['id'],
                    $class['class_id'],
                    $class['level']['current']['level'],
                    $pc['share'],
                    $class['xp_mod'],
                    $class['xp_earned'],
                    $pc['note']
                ];
                $value[] =  '(' . $pdo->quote($values) . ')';
            }
        }
        $stm .= ' ' . implode(", ", $value);
        $makeparty = $pdo->perform($stm, []);
    }
    $v['msg'][$expedition_id] = "<strong>\"" . $expedition['name'] . "\" expedition created!";
} elseif (isset($_POST['char'])) {
    // create a new character
    $makeexpedition = $pdo->fetchAffected(
        'INSERT INTO pcs (name) VALUE (?)',
        [$_POST['char']['name']]
    );

    $pc_id = $pdo->lastInsertId();
    $v['msg'][] = 'Created new character: <a href="/admin?mode=edit&type=pcs&id=' . $pc_id . '">' . $_POST['char']['name'] . '</a>';
}

$stm = 'SELECT id, date, name, xp_treasure, xp_monster, xp_other, dungeon_level, average_party_level FROM expedition';

$expeditions = $pdo->fetchAssoc($stm, []);

foreach ($expeditions as $key => $expedition) {
    $primaryList = [];
    $retainerList = [];

    $stm = 'SELECT pc.id, name, share, note FROM expedition_participants as sp JOIN pcs as pc on sp.pc_id = pc.id WHERE sp.expedition_id = ? ORDER BY share, name';

    $party = $pdo->fetchAssoc($stm, [$expedition['id']]);

    foreach ($party as $pc) {
        $pcDisplay = $pc['name'] . ' (' . $pc['note'] . ')';
        if ($pc['share'] == 1) {
            $primaryList[] = $pcDisplay;
        } else {
            $retainerList[] = $pcDisplay;
        }
    }

    $expeditions[$key]['party'] = $party;
    $expeditions[$key]['partyDisplay'] = implode(', ', $primaryList);
    if (!empty($retainerList)) {
        $expeditions[$key]['partyDisplay'] .= '; ' . implode(', ', $retainerList);
    }
}

$pcs = getMultiplePCwithClassLevel();

foreach ($pcs as $id => $pc) {
    $numberOfClasses = count($pc['class']);

    $display = [];

    foreach ($pc['class'] as $class) {
        $display['class'][] = $class['class'] . ' ' . $class['level']['current']['level'];
        $display['title'][] = $class['level']['current']['title'];
        $display['next_level'][] = number_format($class['level']['next']['xp_required']);
        $display['xp_mod'][] = $class['xp_mod'];
        $display['xp_total'][] = number_format($class['xp_total']);
    }

    foreach ($display as $name => $value) {
        $pcs[$id]['display'][$name] = implode(' / ', $value);
    }
}

ob_start();
?>
<h1>Admin</h1>
<h2>Existing Expeditions</h2>
<table>
    <tr>
        <th rowspan=2>Expedition</th>
        <th rowspan=2>Real Date</th>
        <th colspan=3>XP</th>
        <th rowspan=2>Dungeon Level</th>
        <th rowspan=2>Avg. Party Level</th>
        <th rowspan=2></th>
    </tr>
    <tr>
        <th>Treasure</th>
        <th>Monster</th>
        <th>Other</th>
    </tr>
    <?php foreach ($expeditions as $expedition) { ?>
        <tr>
            <td><?= $expedition['name'] ?></td>
            <td><?= $expedition['date'] ?></td>
            <td><?= $expedition['xp_treasure'] ?></td>
            <td><?= $expedition['xp_monster'] ?></td>
            <td><?= $expedition['xp_other'] ?></td>
            <td><?= $expedition['dungeon_level'] ?></td>
            <td><?= $expedition['average_party_level'] ?></td>
            <td><a href="?mode=edit&type=expedition&id=<?= $expedition['id'] ?>">edit</a></td>
        </tr>
        <tr>
            <td colspan=8>
                <?= $expedition['partyDisplay'] ?>
            </td>
        </tr>
    <?php } ?>
</table>

<form action="" method="post">
    <h2>Create New Expedition</h2>
    <input id="action" name="action" type="hidden" value="admin">
    <label for="expedition[name]">Expedition Name:</label>
    <input type="text" name="expedition[name]" maxlength="50">
    <br><label for="expedition[date]">Game Date:</label>
    <input type="date" name="expedition[date]">
    <br><label for="expedition[xp_treasure]">XP from Treasure:</label>
    <input type="number" name="expedition[xp_treasure]" min=0 value=0>
    <br><label for="expedition[xp_monster]">XP from Monsters:</label>
    <input type="number" name="expedition[xp_monster]" min=0 value=0>
    <br><label for="expedition[xp_other]">XP from Other Source:</label>
    <input type="number" name="expedition[xp_other]" min=0 value=0>
    <br><label for="expedition[dungeon_level]">Dungeon Level:</label>
    <input type="number" name="expedition[dungeon_level]" min=0 value=1>
    <br><label for="expedition[primary]">Party:</label>
    <br><select name="expedition[primary][]" multiple>
        <?php foreach ($pcs as $pc) {
            echo '<option value="' . $pc['id'] . '">' . $pc['name'] . '</option>';
        } ?>
    </select>
    <br><label for="expedition[retainer]">Retainers:</label>
    <br><select name="expedition[retainer][]" multiple>
        <?php foreach ($pcs as $pc) {
            echo '<option value="' . $pc['id'] . '">' . $pc['name'] . '</option>';
        } ?>
    </select>
    <br><input type="submit" value="Create expedition!">
</form>

<h2>Create New Character</h2>
<form action="" method="post">
    <input id="action" name="action" type="hidden" value="admin">
    <label for="char[name]">Character Name:</label>
    <input type="text" name="char[name]">
    <br><input type="submit" value="Create character!">
</form>

<h2>Edit Characters</h2>
<p><a href="?mode=export&type=pcs">export</a></p>
<table>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Title</th>
        <th>Class/Level</th>
        <th>Total XP</th>
        <th>XP for Next Level</th>
        <th>XP Mod</th>
        <th></th>
    </tr>
    <?php foreach ($pcs as $pc) { ?>
        <tr>
            <td><?= $pc['id'] ?></td>
            <td><?= $pc['name'] ?></td>
            <td><?= $pc['display']['title'] ?></td>
            <td><?= $pc['display']['class'] ?></td>
            <td><?= $pc['display']['xp_total'] ?></td>
            <td><?= $pc['display']['next_level'] ?></td>
            <td><?= $pc['display']['xp_mod'] ?></td>
            <td><a href="?mode=edit&type=pcs&id=<?= $pc['id'] ?>">edit</a></td>
        </tr>
    <?php } ?>
</table>
<?php
$v['body'] = ob_get_clean();
