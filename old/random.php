<?php
require_once __DIR__ . '/bootstrap.php';

$table = $_GET['table'];

$v['title'] = "Random from Table";

$v['body'] = "<h1>Random from Table</h1>";

$v['body'] .= '<p>Table requested: '.$table.'</p>';

$v['body'] .= '<p><strong>Result:</strong></p>';

$v['body'] .= parseTable($table);
