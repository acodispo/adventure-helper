<?php
require_once __DIR__ . '/bootstrap.php';
$pdo = db();

$v['body'] = '<a href="/admin">Back to Admin</a>';
if ($_GET['type'] == 'pcs') {
    if (isset($_POST['action']) && $_POST['action'] == 'edit') {
        $edited = $_POST['char'];
        $edited['id'] = $_GET['id'];

        if (!empty($edited['addClass'])) {
            $pc = getSinglePCwithClassLevel($edited['id']);
            if (empty($edited['addXPMod'])) {
                $v['msg'][] = "You must provide an XP modifier for the new class.";
            } else {
                if (array_key_exists($edited['addClass'], $pc['class'])) {
                    $v['msg'][] = "Character already has that class!";
                } elseif (!array_search($edited['addXPMod'], $config['xpMods'])) {
                    $v['msg'][] = "That is not an allowed XP mod.";
                } else {
                    $result = addClassToPC((int) $edited['id'], (int) $edited['addClass'], (float) $edited['addXPMod']);
                    if (empty($result)) {
                        $v['msg'][] = "Problem adding class.";
                    } else {
                        $v['msg'][] = "Class successfully added.";
                    }
                }
            }
        }

        // update status
        $stm = "UPDATE pcs SET status = :status WHERE id = :id";
        $row_count = $pdo->fetchAffected($stm, $edited);
    }

    $pc = getSinglePCwithClassLevel($_GET['id']);

    $i = count($pc['class']);

    $classDisplay = "";
    foreach ($pc['class'] as $class) {
        $classDisplay .= $class['class'] . ' ' . $class['level']['current']['level'];
        $i--;
        if ($i > 0) $classDisplay .= ' / ';
    }

    $pc['addableClasses'] = getAddableClassesForPC($pc['id']);

    $v['title'] = "Edit Player Character";
    $v['body'] .= "<h1>" . $v['title'] . "</h1>";

    ob_start();
?>
    <form action="?type=<?= $_GET['type'] ?>&id=<?= $_GET['id'] ?>" method="post">
        <ul>
            <li><strong>ID:</strong> <?= $pc['id'] ?></li>
            <li><strong>Name:</strong> <?= $pc['name'] ?></li>
            <li>
                <strong>Class/Level:</strong> <?= $classDisplay ?>
                <ul>
                    <li>
                        <strong><label for="char[addClass]">Add class:</label>
                            <select name="char[addClass]">
                                <option value="0" selected>None</option>
                                <?php foreach ($pc['addableClasses'] as $addableClass) { ?>
                                    <option value="<?= $addableClass['id'] ?>"><?= $addableClass['name'] ?></option>
                                <?php } ?>
                            </select>
                            <label for="char[addXPMod]">XP Mod:</label>
                            <input type="number" step=".01" name="char[addXPMod]" value="1.00">
                    </li>
                </ul>
            </li>
            <li>
                <strong><label for="char[status]">Status:</label></strong>
                <select name="char[status]">
                    <option value="active" <?php if ($pc['status'] == 'active') echo ' selected'; ?>>Active</option>
                    <option value="inactive" <?php if ($pc['status'] == 'inactive') echo ' selected'; ?>>Inactive</option>
                </select>
            </li>
        </ul>
        <input id="action" name="action" type="hidden" value="edit">
        <br><input type="submit" value="Save Changes">
    </form>

<?php
    $v['body'] .= ob_get_clean();
}

if ($_GET['type'] == 'expedition') {
    if (isset($_POST['action']) && $_POST['action'] == 'edit') {
        $edited = $_POST['expedition'];
        $edited['id'] = $_GET['id'];

        $stm = "UPDATE expedition SET date = :date, xp_treasure = :xp_treasure, xp_monster = :xp_monster, xp_other = :xp_other, dungeon_level = :dungeon_level WHERE id = :id";
        $row_count = $pdo->fetchAffected($stm, $edited);

        foreach ($edited['party'] as $id => $pc) {
            $pc['id'] = $id;
            $pc['expedition_id'] = $edited['id'];
            $stm = "UPDATE expedition_participants SET share = :share, xp_awarded = :xp_awarded WHERE pc_id = :id AND expedition_id = :expedition_id";
            $row_count = $pdo->fetchAffected($stm, $pc);
        }
    }

    $stm = 'SELECT id, date, xp_treasure, xp_monster, xp_other, dungeon_level FROM expedition WHERE id = ?';

    $expedition = $pdo->fetchOne($stm, [$_GET['id']]);

    $stm = 'SELECT pc.id, name, share, xp_awarded FROM expedition_participants as sp JOIN pcs as pc on sp.pc_id = pc.id WHERE sp.expedition_id = ? ORDER BY share DESC, name';
    $expedition['party'] = $pdo->fetchAssoc($stm, [$expedition['id']]);

    $v['title'] = "Edit Expedition";
    $v['body'] .= "<h1>" . $v['title'] . "</h1>";

    ob_start();
?>
    <form action="?type=<?= $_GET['type'] ?>&id=<?= $_GET['id'] ?>" method="post">
        <strong>ID:</strong> <?= $expedition['id'] ?>
        <br><strong>Date:</strong> <?= $expedition['date'] ?>
        <br><label for="expedition[date]">Game Date:</label>
        <input type="date" name="expedition[date]" value="<?= $expedition['date'] ?>">
        <br><label for="expedition[xp_treasure]">XP from Treasure:</label>
        <input type="number" name="expedition[xp_treasure]" value="<?= $expedition['xp_treasure'] ?>" step=1>
        <br><label for="expedition[xp_monster]">XP from Monsters:</label>
        <input type="number" name="expedition[xp_monster]" value="<?= $expedition['xp_monster'] ?>" step=1>
        <br><label for="expedition[xp_other]">XP from Other Source:</label>
        <input type="number" name="expedition[xp_other]" value="<?= $expedition['xp_other'] ?>" step=1>
        <br><label for="expedition[dungeon_level]">Dungeon Level:</label>
        <input type="number" name="expedition[dungeon_level]" value="<?= $expedition['dungeon_level'] ?>" step=1>
        <p><strong>Party:</strong></p>
        <ol>
            <?php foreach ($expedition['party'] as $pc) { ?>
                <li>
                    <strong><?= $pc['name'] ?></strong>
                    <ul>
                        <li>
                            <strong><label for="expedition[party][<?= $pc['id'] ?>][share]">Share:</label></strong>
                            <input name="expedition[party][<?= $pc['id'] ?>][share]" type="number" max=1 min=".5" step=".5" value="<?= $pc['share'] ?>">
                        </li>
                        <li>
                            <strong><label for="expedition[party][<?= $pc['id'] ?>][xp_awarded]">XP Awarded:</label></strong>
                            <input name="expedition[party][<?= $pc['id'] ?>][xp_awarded]" type="number" step="1" value="<?= $pc['xp_awarded'] ?>">
                        </li>
                    </ul>
                </li>
            <?php } ?>
        </ol>
        <input id="action" name="action" type="hidden" value="edit">
        <p><input type="submit" value="Save Changes"></p>
    </form>

<?php
    $v['body'] .= ob_get_clean();
}
