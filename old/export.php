<?php
require_once __DIR__ . '/bootstrap.php';
$pdo = db();

$v['title'] = "Export";
$v['body'] = '<a href="/admin">Back to Admin</a>';
if ($_GET['type'] == 'pcs') {
    $pcs = getMultiplePCwithClassLevel();

    foreach ($pcs as $id => $pc) {
        $numberOfClasses = count($pc['class']);

        $display = [];

        $display['name'][] = '[[' . $pc['name'] . ']]';

        foreach ($pc['class'] as $class) {
            $display['title'][] = $class['level']['current']['title'];
            $display['class'][] = $class['class'] . ' ' . $class['level']['current']['level'];
        }

        foreach ($display as $name => $value) {
            $combinedDisplay[$name] = implode(' / ', $value);
        }
            $display['next_level'][] = number_format($class['level']['next']['xp_required']);
            $display['xp_mod'][] = $class['xp_mod'];
            $display['xp_total'][] = number_format($class['xp_total']);


            $pcs[$id]['display'] = '| ' . implode(' | ', $combinedDisplay) . ' |';
    }


    $v['body'] .= "<h1>" . $v['title'] . "</h1>";

    ob_start();
    dump($pcs);
?>
    <pre><code>

| Character | Title | XP | Level | Location | Status | Player | Last Active |
<?php foreach ($pcs as $pc) { echo $pc['display'] . '<br>'; ?>
<?php } ?>
| [[Bemdath Hilfer]]        | Myrmidon       | 7,166 / 8,000 (+5%)        | Fighter 3            | [[Haven Town]] | active   | ThatOneDrow               | 2020-10-17     |
| [[Astrid]]                | Eriff / Medium | 1,540 / 2,250; 316 / 2,400 | Thief 1 / Sorcerer 1 | [[Haven Town]] | active   | iattackthedarkness        | 2020-10-17     |
| [[Lyyndbrliatheruxalter]] | 0-level        | 632 / ???                  |                      | [[Haven Town]] | active   | ThatOneDrow               | 2020-10-17     |
</code></pre>

<?php
    $v['body'] .= ob_get_clean();
}

if ($_GET['type'] == 'expedition') {
    if (isset($_POST['action']) && $_POST['action'] == 'edit') {
        $edited = $_POST['expedition'];
        $edited['id'] = $_GET['id'];

        $stm = "UPDATE expedition SET date = :date, xp_treasure = :xp_treasure, xp_monster = :xp_monster, xp_other = :xp_other, dungeon_level = :dungeon_level WHERE id = :id";
        $row_count = $pdo->fetchAffected($stm, $edited);

        foreach ($edited['party'] as $id => $pc) {
            $pc['id'] = $id;
            $pc['expedition_id'] = $edited['id'];
            $stm = "UPDATE expedition_participants SET share = :share, xp_awarded = :xp_awarded WHERE pc_id = :id AND expedition_id = :expedition_id";
            $row_count = $pdo->fetchAffected($stm, $pc);
        }
    }

    $stm = 'SELECT id, date, xp_treasure, xp_monster, xp_other, dungeon_level FROM expedition WHERE id = ?';

    $expedition = $pdo->fetchOne($stm, [$_GET['id']]);

    $stm = 'SELECT pc.id, name, share, xp_awarded FROM expedition_participants as sp JOIN pcs as pc on sp.pc_id = pc.id WHERE sp.expedition_id = ? ORDER BY share DESC, name';
    $expedition['party'] = $pdo->fetchAssoc($stm, [$expedition['id']]);

    $v['title'] = "Edit Expedition";
    $v['body'] .= "<h1>" . $v['title'] . "</h1>";

    ob_start();
?>
    <form action="?type=<?= $_GET['type'] ?>&id=<?= $_GET['id'] ?>" method="post">
        <strong>ID:</strong> <?= $expedition['id'] ?>
        <br><strong>Date:</strong> <?= $expedition['date'] ?>
        <br><label for="expedition[date]">Game Date:</label>
        <input type="date" name="expedition[date]" value="<?= $expedition['date'] ?>">
        <br><label for="expedition[xp_treasure]">XP from Treasure:</label>
        <input type="number" name="expedition[xp_treasure]" value="<?= $expedition['xp_treasure'] ?>" step=1>
        <br><label for="expedition[xp_monster]">XP from Monsters:</label>
        <input type="number" name="expedition[xp_monster]" value="<?= $expedition['xp_monster'] ?>" step=1>
        <br><label for="expedition[xp_other]">XP from Other Source:</label>
        <input type="number" name="expedition[xp_other]" value="<?= $expedition['xp_other'] ?>" step=1>
        <br><label for="expedition[dungeon_level]">Dungeon Level:</label>
        <input type="number" name="expedition[dungeon_level]" value="<?= $expedition['dungeon_level'] ?>" step=1>
        <p><strong>Party:</strong></p>
        <ol>
            <?php foreach ($expedition['party'] as $pc) { ?>
                <li>
                    <strong><?= $pc['name'] ?></strong>
                    <ul>
                        <li>
                            <strong><label for="expedition[party][<?= $pc['id'] ?>][share]">Share:</label></strong>
                            <input name="expedition[party][<?= $pc['id'] ?>][share]" type="number" max=1 min=".5" step=".5" value="<?= $pc['share'] ?>">
                        </li>
                        <li>
                            <strong><label for="expedition[party][<?= $pc['id'] ?>][xp_awarded]">XP Awarded:</label></strong>
                            <input name="expedition[party][<?= $pc['id'] ?>][xp_awarded]" type="number" step="1" value="<?= $pc['xp_awarded'] ?>">
                        </li>
                    </ul>
                </li>
            <?php } ?>
        </ol>
        <input id="action" name="action" type="hidden" value="edit">
        <p><input type="submit" value="Save Changes"></p>
    </form>

<?php
    $v['body'] .= ob_get_clean();
}
