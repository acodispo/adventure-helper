<?php
require_once __DIR__ . '/bootstrap.php';
$v['title'] = "Solo Delve";

$table = $_GET['table'] ?? null;
$result = [];

function soloTableOne()
{
    $r = roll('1d20');
    switch ($r) {
    case ($r>0 && $r<4):
        $result = '60ft straight';
        $next = 1;
        break;
    case ($r>3 && $r<8):
        $result = false;
        $table = 2;
        break;
    case ($r>7 && $r<11):
        break;

    };
}

function soloTableTwo()
{
    $r = roll('1d12');
    dump($r);

    switch ($r) {
    case ($r>0 && $r<5):
        $result = 'Door on left';
        break;
    case ($r>4 && $r<9):
        $result = 'Door on right';
        break;
    case ($r>8 && $r<13):
        $result = 'Door ahead';
        break;
    }

    $result .= ', beyond is: ';

    $r = roll('1d12');
    dump($r);

    switch ($r) {
    case ($r>0 && $r<3):
        $result .= 'parallel corridor or 10ft by 10ft room (for door ahead)';
        break;
    case ($r == 3):
        $result .= 'passage straight 30ft';
        break;
    case ($r == 4):
        $result .= 'passage 45 deg. left';
        break;
    case ($r == 5):
        $result .= 'passage 45 deg. right';
        break;
    case ($r > 5):
        $result .= soloTableFiveRoom();
    }

    return $result;
}

function soloTableFiveRoom()
{
    $r = roll('1d12');
    switch ($r) {
    case ($r == 1):
        $result = "10ft x 10ft room";
        break;
    case ($r > 1 && $r < 5): 
        $result = "20ft x 20ft room";
        break;
    case ($r == 5):
        $result = "30ft x 30ft room";
        break;
    case ($r == 6):
        $result = "40ft x 40ft room";
        break;
    case ($r == 7):
        $result = "10ft x 20ft room";
        break;
    case ($r > 7 && $r < 10): 
        $result = "20ft x 30ft room";
        break;
    case ($r == 10):
        $result = "20ft x 40ft room";
        break;
    case ($r == 11):
        $result = "30ft x 40ft room";
        break;
    case ($r == 12):
        $result = soloUnusualSizeShape();
    }

    return $result;
}

function soloUnusualSizeShape()
{
    return "room of unusual size and shape";
}

dump(soloTableTwo());
exit;

if ($table) {
    while ($table == 1) {
    }
}


    ob_start();
?>
# Solo Adventure

Use these links to create a solo adventure.

| [Periodic](?table=1) | |
<?php
    $body = ob_get_clean();
    $body .= $result;
    $Parsedown = new Parsedown();
    $v['body'] = $Parsedown->text($body);

