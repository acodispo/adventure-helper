<?php
$v['out'] = (!isset($v['out'])) ? 'html' : $v['out'];
if($v['out'] == "text") {
   header('Content-Type: text/plain');
   echo $v['body'];
} else {
   header('Content-Type: text/html');
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <title><?= $v['title'] ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
      <link rel="stylesheet" href="/style.css">
      <style>
      body {
          max-width:60em;
      }
      </style>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   </head>
   <body>
       <?php if (isset($v['msg'])) {
           echo '<hr>';
           echo '<strong>Messages</strong>';
           foreach ($v['msg'] as $message) {
               echo '<p>'.$message.'</p>';
           }
           echo '<hr>';
       } ?>
      <?= $v['body'] ?>
   </body>
</html>
<?php
}
