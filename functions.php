<?php
use Aura\Sql\ExtendedPdo;

function loadlib()
{
    require __DIR__ . '/lib.php';
    return $lib;
}
function db()
{
    global $config;
    $pdo = new ExtendedPdo(
        'mysql:host=' . $config['db']['hostname'] . ';dbname=' . $config['db']['database'],
        $config['db']['username'],
        $config['db']['password'],
        [], // driver attributes/options as key-value pairs
        []  // queries to execute after connection
    );
    return $pdo;
}

function getClassLevelForPC(string $pc_id)
{
    $pdo = db();
    $classes = $pdo->fetchAssoc(
        'SELECT pc_class.class_id, rules_classes.name as class, pc_class.xp_mod
        FROM pc_class
        JOIN rules_classes ON pc_class.class_id = rules_classes.id
        WHERE pc_class.pc_id = ?',
        [
            $pc_id
        ]
    );

    $numberOfClasses = count($classes);

    $bind['pc_id'] = $pc_id;

    $apprenticeXP = $pdo->fetchValue(
        "SELECT SUM(xp_awarded) FROM expedition_participants WHERE pc_id = :pc_id AND pc_class = 4",
        $bind
    );

    if ($numberOfClasses === 0) {
        $classes = [
            4 => [
                "class_id" => "4",
                "class" => "Apprentice",
                "xp_mod" => "1.00",
                "xp_total" => $apprenticeXP,
                "level" => [
                    "current" => [
                        "level" => "0",
                        "title" => "Apprentice",
                        "xp_required" => "0"
                    ],
                    "next" => [
                        "level" => "0",
                        "title" => "Apprentice",
                        "xp_required" => "0"
                    ],
                ]
            ]
        ];
    } else {
        foreach ($classes as $id => $class) {
            $bind['class_id'] = $id;
            $xpEarnedForClass = $pdo->fetchValue(
                "SELECT SUM(xp_awarded) FROM expedition_participants WHERE pc_id = :pc_id AND pc_class = :class_id",
                $bind
            );
            $classes[$id]['xp_total'] = ($apprenticeXP / $numberOfClasses) + $xpEarnedForClass;
            $classes[$id]['level']['current'] = $pdo->fetchOne(
                'SELECT level, title, xp_required
                FROM rules_levels
                WHERE class_id = ?
                AND xp_required <= ?
                ORDER BY xp_required DESC
                LIMIT 1',
                [
                    $id,
                    $classes[$id]['xp_total']
                ]
            );
            $classes[$id]['level']['next'] = $pdo->fetchOne(
                'SELECT level, title, xp_required
                FROM rules_levels
                WHERE class_id = ?
                AND xp_required > ?
                ORDER BY xp_required ASC
                LIMIT 1',
                [
                    $id,
                    $classes[$id]['xp_total']
                ]
            );
        }
    }
    return $classes;
}

function getMultiplePCwithClassLevel(string $ids = null, array $criteria = ['status = ', 'active'])
{
    $pdo = db();

    if (is_string($ids)) {
        $id_clause = "AND pcs.id IN ($ids)";
    } else {
        $id_clause = "";
    }

    $stm = "SELECT pcs.id, pcs.name as name, SUM(xp_awarded) as total_xp
    FROM pcs
    LEFT JOIN expedition_participants sp on pcs.id = sp.pc_id
    WHERE " . $criteria[0] . " ?
    $id_clause
    GROUP BY pcs.id
    ORDER BY name
    ";
    $bind = [
        $criteria[1]
    ];
    $pcs = $pdo->fetchAssoc($stm, $bind);

    foreach ($pcs as $id => $pc) {
        $pcs[$id]['class'] = getClassLevelForPC($id);
    }

    return $pcs;
}

function addClassToPC(int $pc_id, int $class_id, float $xp_mod)
{
    $pdo = db();
    $stm = 'INSERT INTO pc_class (pc_id, class_id, xp_mod) VALUES (?, ?, ?)';
    $bind = [
        $pc_id,
        $class_id,
        $xp_mod
    ];
    return $pdo->fetchAffected($stm, $bind);
}

function getAddableClassesForPC($pc_id)
{
    $pdo = db();
    $stm = 'SELECT id, name FROM rules_classes WHERE id NOT IN (SELECT class_id FROM pc_class WHERE pc_id = ?) AND id != 4';
    $bind = [$pc_id];
    return $pdo->fetchAssoc($stm, $bind);
}

function getSinglePCwithClassLevel($pc_id)
{
    $pdo = db();

    $pc = $pdo->fetchOne(
        'SELECT pcs.id, pcs.name, pcs.status, SUM(xp_awarded) as total_xp
        FROM pcs
        LEFT JOIN expedition_participants sp on pcs.id = sp.pc_id
        WHERE pcs.id = ?
        GROUP BY pcs.id',
        [$pc_id]
    );

    $pc['class'] = getClassLevelForPC($pc_id);

    return $pc;
}