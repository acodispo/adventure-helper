<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExpeditionParticipants
 *
 * @ORM\Table(name="expedition_participants", indexes={@ORM\Index(name="expedition_id", columns={"expedition_id"}), @ORM\Index(name="pc_id", columns={"pc_id"})})
 * @ORM\Entity
 */
class ExpeditionParticipants
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="pc_class", type="integer", nullable=false)
     */
    private $pcClass;

    /**
     * @var int
     *
     * @ORM\Column(name="pc_level", type="integer", nullable=false)
     */
    private $pcLevel;

    /**
     * @var string
     *
     * @ORM\Column(name="share", type="decimal", precision=3, scale=2, nullable=false, options={"default"="1.00"})
     */
    private $share = '1.00';

    /**
     * @var string
     *
     * @ORM\Column(name="xp_mod", type="decimal", precision=3, scale=2, nullable=false)
     */
    private $xpMod;

    /**
     * @var int
     *
     * @ORM\Column(name="xp_awarded", type="integer", nullable=false)
     */
    private $xpAwarded = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", length=65535, nullable=false)
     */
    private $note;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts", type="datetime", nullable=false, options={"default"="current_timestamp()"})
     */
    private $ts = 'current_timestamp()';

    /**
     * @var \Expedition
     *
     * @ORM\ManyToOne(targetEntity="Expedition")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="expedition_id", referencedColumnName="id")
     * })
     */
    private $expedition;

    /**
     * @var \Pcs
     *
     * @ORM\ManyToOne(targetEntity="Pcs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pc_id", referencedColumnName="id")
     * })
     */
    private $pc;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPcClass(): ?int
    {
        return $this->pcClass;
    }

    public function setPcClass(int $pcClass): self
    {
        $this->pcClass = $pcClass;

        return $this;
    }

    public function getPcLevel(): ?int
    {
        return $this->pcLevel;
    }

    public function setPcLevel(int $pcLevel): self
    {
        $this->pcLevel = $pcLevel;

        return $this;
    }

    public function getShare(): ?string
    {
        return $this->share;
    }

    public function setShare(string $share): self
    {
        $this->share = $share;

        return $this;
    }

    public function getXpMod(): ?string
    {
        return $this->xpMod;
    }

    public function setXpMod(string $xpMod): self
    {
        $this->xpMod = $xpMod;

        return $this;
    }

    public function getXpAwarded(): ?int
    {
        return $this->xpAwarded;
    }

    public function setXpAwarded(int $xpAwarded): self
    {
        $this->xpAwarded = $xpAwarded;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getTs(): ?\DateTimeInterface
    {
        return $this->ts;
    }

    public function setTs(\DateTimeInterface $ts): self
    {
        $this->ts = $ts;

        return $this;
    }

    public function getExpedition(): ?Expedition
    {
        return $this->expedition;
    }

    public function setExpedition(?Expedition $expedition): self
    {
        $this->expedition = $expedition;

        return $this;
    }

    public function getPc(): ?Pcs
    {
        return $this->pc;
    }

    public function setPc(?Pcs $pc): self
    {
        $this->pc = $pc;

        return $this;
    }


}
