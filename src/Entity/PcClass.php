<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PcClass
 *
 * @ORM\Table(name="pc_class", indexes={@ORM\Index(name="pcid", columns={"pc_id"})})
 * @ORM\Entity
 */
class PcClass
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="class_id", type="integer", nullable=false)
     */
    private $classId;

    /**
     * @var string
     *
     * @ORM\Column(name="xp_mod", type="decimal", precision=3, scale=2, nullable=false, options={"default"="1.00"})
     */
    private $xpMod = '1.00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts", type="datetime", nullable=false, options={"default"="current_timestamp()"})
     */
    private $ts = 'current_timestamp()';

    /**
     * @var \Pcs
     *
     * @ORM\ManyToOne(targetEntity="Pcs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pc_id", referencedColumnName="id")
     * })
     */
    private $pc;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClassId(): ?int
    {
        return $this->classId;
    }

    public function setClassId(int $classId): self
    {
        $this->classId = $classId;

        return $this;
    }

    public function getXpMod(): ?string
    {
        return $this->xpMod;
    }

    public function setXpMod(string $xpMod): self
    {
        $this->xpMod = $xpMod;

        return $this;
    }

    public function getTs(): ?\DateTimeInterface
    {
        return $this->ts;
    }

    public function setTs(\DateTimeInterface $ts): self
    {
        $this->ts = $ts;

        return $this;
    }

    public function getPc(): ?Pcs
    {
        return $this->pc;
    }

    public function setPc(?Pcs $pc): self
    {
        $this->pc = $pc;

        return $this;
    }


}
