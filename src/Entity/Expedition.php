<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Expedition
 *
 * @ORM\Table(name="expedition")
 * @ORM\Entity(repositoryClass="App\Repository\ExpeditionRepository")
 */
class Expedition
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="xp_treasure", type="integer", nullable=false)
     */
    private $xpTreasure = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="xp_monster", type="integer", nullable=false)
     */
    private $xpMonster = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="xp_other", type="integer", nullable=false)
     */
    private $xpOther = '0';

    /**
     * @var int
     *
     * @ORM\Column(name="dungeon_level", type="integer", nullable=false, options={"default"="1"})
     */
    private $dungeonLevel = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="average_party_level", type="integer", nullable=false)
     */
    private $averagePartyLevel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts", type="datetime", nullable=false, options={"default"="current_timestamp()"})
     */
    private $ts = 'current_timestamp()';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getXpTreasure(): ?int
    {
        return $this->xpTreasure;
    }

    public function setXpTreasure(int $xpTreasure): self
    {
        $this->xpTreasure = $xpTreasure;

        return $this;
    }

    public function getXpMonster(): ?int
    {
        return $this->xpMonster;
    }

    public function setXpMonster(int $xpMonster): self
    {
        $this->xpMonster = $xpMonster;

        return $this;
    }

    public function getXpOther(): ?int
    {
        return $this->xpOther;
    }

    public function setXpOther(int $xpOther): self
    {
        $this->xpOther = $xpOther;

        return $this;
    }

    public function getDungeonLevel(): ?int
    {
        return $this->dungeonLevel;
    }

    public function setDungeonLevel(int $dungeonLevel): self
    {
        $this->dungeonLevel = $dungeonLevel;

        return $this;
    }

    public function getAveragePartyLevel(): ?int
    {
        return $this->averagePartyLevel;
    }

    public function setAveragePartyLevel(int $averagePartyLevel): self
    {
        $this->averagePartyLevel = $averagePartyLevel;

        return $this;
    }

    public function getTs(): ?\DateTimeInterface
    {
        return $this->ts;
    }

    public function setTs(\DateTimeInterface $ts): self
    {
        $this->ts = $ts;

        return $this;
    }

    public function setParty(array $party): self
    {
        $this->party = $party;

        return $this;
    }

    public function getParty(): ?array
    {
        return $this->party;
    }


}
