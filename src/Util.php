<?php

namespace App {
    use DiceBag\DiceBag;

    class Util
    {
        function __construct($libpath = __DIR__ . '/../lib.php')
        {
            require $libpath;
            require __DIR__ . '/../config/config.php';
            $this->config = $config;
            $this->lib = $lib;
        }

        function roll($dice)
        {
            return DiceBag::factory($dice)->getTotal();
        }

        function parseTable($table, $output = "md")
        {
            $hexdescribe = str_replace('[table name here]', '[' . $table . ']', $this->config['hexdescribe']);
            exec($hexdescribe, $out);
            if ($output == 'html') {
                $Parsedown = new Parsedown();
                $rtn = $Parsedown->text($out[0]);
            } else {
                $rtn = $out[0];
            }
            return $rtn;
        }
    }
}
