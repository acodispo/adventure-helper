<?php

namespace App\Service {
    class Library
    {
        function __construct($libpath = __DIR__ . '/../../lib.php')
        {
            require $libpath;
            $this->lib = $lib;
        }

        public function getRand(string $table)
        {
            return $this->getRandFrom($this->lib[$table]);
        }

        private function humanNamePrefix()
        {
            return $this->getRandFrom($this->lib['humanNamePrefix']);
        }

        private function humanNameSuffix()
        {
            return $this->getRandFrom($this->lib['humanNameSuffix']);
        }
        private function humanNameFull()
        {
            return $this->getRandFrom($this->lib['humanNameFull']);
        }
        private function humanName1()
        {
            switch ($this->getRandomWeightedElement([4, 5, 1])) {
                case 0:
                    return $this->humanNamePrefix();
                    break;
                case 1:
                    return $this->humanNamePrefix() . $this->humanNameSuffix();
                    break;
                case 2:
                    return $this->humanNameFull();
                    break;
            }
        }

        private function humanName2()
        {
            $num = mt_rand(1, 2);
            switch ($num) {
                case 1:
                    return $this->humanName1() . 'childe';
                    break;
                case 2:
                    return $this->humanName1();
                    break;
            }
        }

        public function humanName()
        {

            switch ($this->getRandomWeightedElement([3, 2, 2])) {
                case 0:
                    return $this->humanName1();
                    break;
                case 1:
                    return $this->humanName1() . ' ' . $this->humanName2();
                    break;
                case 2:
                    return $this->humanName1() . ' the ' . $this->getRandFrom($this->lib['epithet']);
                    break;
            }
        }

        private function getRandFrom(array $table)
        {
            shuffle($table);
            return array_pop($table);
        }

        /**
         * getRandomWeightedElement()
         * from: https://stackoverflow.com/a/11872928/14545336
         * Utility function for getting random values with weighting.
         * Pass in an associative array, such as array('A'=>5, 'B'=>45, 'C'=>50)
         * An array like this means that "A" has a 5% chance of being selected, "B" 45%, and "C" 50%.
         * The return value is the array key, A, B, or C in this case.  Note that the values assigned
         * do not have to be percentages.  The values are simply relative to each other.  If one value
         * weight was 2, and the other weight of 1, the value with the weight of 2 has about a 66%
         * chance of being selected.  Also note that weights should be integers.
         * 
         * @param array $weightedValues
         */
        private function getRandomWeightedElement(array $weightedValues)
        {
            $rand = mt_rand(1, (int) array_sum($weightedValues));

            foreach ($weightedValues as $key => $value) {
                $rand -= $value;
                if ($rand <= 0) {
                    return $key;
                }
            }
        }
    }
}
