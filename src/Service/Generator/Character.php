<?php

namespace App\Service\Generator {

    use App\Service\Generator\Character\Wittermixe;
    use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
    use App\Service\Generator\Character\Game1974;

    class Character
    {
        private $router;
        public $name;

        public function __construct(UrlGeneratorInterface $router)
        {
            $this->router = $router;
        }
        public function generate(string $game = '1974')
        {
            switch ($game) {
                case 'wittermixe':
                    $generator = new Wittermixe($this->router);
                    break;
                case '1974':
                    $generator = new Game1974($this->router);
                    break;
                default:
                    $generator = new Game1974($this->router);
            }

            $generator->generateCharacter();
            foreach ($generator->char as $k => $v) {
                $this->$k = $v;
            }
        }

        public function getCharacter()
        {
            return (array) $this;
        }
    }
}
