<?php

/**
 * The game rules used by the 'Game1974' class to generate characters' ability scores and class are Open Game Content under the Open Game License.
 * 
 * A copy of the license is distributed in the root directory of this repository.
 */

namespace App\Service\Generator\Character {

    use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

    class Game1974 extends Base
    {
        public function __construct(UrlGeneratorInterface $router)
        {
            $this->router = $router;
            $this->util = new \App\Util();
        }
        protected function getGame()
        {
            return '1974';
        }
        protected function randomTrait()
        {
            $class = $this->library->getRand('classesLbb');
            $this->charClass = $class;
            return $class;
        }
        protected function getHD()
        {
            switch ($this->charClass) {
                case 'Fighter':
                    return '1d6+2';
                    break;
                default:
                    return '1d6';
            }
        }
        protected function getLicense()
        {
            return "The game rules used to generate this character's ability scores and class are *Open Game Content* under the [Open Game License](" . $this->router->generate("ogl") . ').';
        }
        protected function assignAbilityModifiers()
        {
            // Intelligence
            if ($this->char['abilities']['int']['score'] > 10) {
                $this->char['abilities']['int']['mod'] = $this->char['abilities']['int']['score'] - 10;
            }

            // Constitution
            if ($this->char['abilities']['con']['score'] < 7) {
                $this->char['abilities']['con']['mod'] = -1;
            } elseif ($this->char['abilities']['con']['score'] > 14) {
                $this->char['abilities']['con']['mod'] = 1;
            }

            // Dexterity
            if ($this->char['abilities']['dex']['score'] < 7) {
                $this->char['abilities']['dex']['mod'] = [
                    'initiative' => -1,
                    'toHitWithMissiles' => -1
                ];
                $this->char['abilities']['dex']['disp'] = " (Init: -1, Missiles: -1)";
            } elseif ($this->char['abilities']['dex']['score'] > 6 and $this->char['abilities']['dex']['score'] < 8) {
                $this->char['abilities']['dex']['mod'] = -1;
                $this->char['abilities']['dex']['disp'] = " (Missiles: -1)";
            } elseif ($this->char['abilities']['dex']['score'] > 12 and $this->char['abilities']['dex']['score'] < 15) {
                $this->char['abilities']['dex']['mod'] = 1;
                $this->char['abilities']['dex']['disp'] = " (Missiles: +1)";
            } elseif ($this->char['abilities']['dex']['score'] > 14 and $this->char['abilities']['dex']['score'] < 19) {
                $this->char['abilities']['dex']['mod'] = [
                    'initiative' => 1,
                    'toHitWithMissiles' => 1
                ];
                $this->char['abilities']['dex']['disp'] = " (Init: +1, Missiles: +1)";
            }

            // Charisma
            if ($this->char['abilities']['cha']['score'] < 6) {
                $this->char['abilities']['cha']['mod'] = [
                    'loyalty' => -2,
                    'reaction' => -1
                ];
                $this->char['abilities']['cha']['disp'] = " (Loyalty: -2, Reaction: -1)";
            } elseif ($this->char['abilities']['cha']['score'] > 5 and $this->char['abilities']['cha']['score'] < 9) {
                $this->char['abilities']['cha']['mod'] = [
                    'loyalty' => -1,
                    'reaction' => -1
                ];
                $this->char['abilities']['cha']['disp'] = " (Loyalty: -1, Reaction: -1)";
            } elseif ($this->char['abilities']['cha']['score'] > 12 and $this->char['abilities']['cha']['score'] < 15) {
                $this->char['abilities']['cha']['mod'] = [
                    'loyalty' => 1,
                    'reaction' => 1
                ];
                $this->char['abilities']['cha']['disp'] = " (Loyalty: +1, Reaction: +1)";
            } elseif ($this->char['abilities']['cha']['score'] > 14 and $this->char['abilities']['cha']['score'] < 18) {
                $this->char['abilities']['cha']['mod'] = [
                    'loyalty' => 2,
                    'reaction' => 1
                ];
                $this->char['abilities']['cha']['disp'] = " (Loyalty: +2, Reaction: +1)";
            } elseif ($this->char['abilities']['cha']['score'] == 18) {
                $this->char['abilities']['cha']['mod'] = [
                    'loyalty' => 4,
                    'reaction' => 1
                ];
                $this->char['abilities']['cha']['disp'] = " (Loyalty: +4, Reaction: +1)";
            }

            foreach ($this->char['abilities'] as $key => $ability) {
                $mod = $ability['mod'] ?? 0;

                if (empty($ability['disp'])) {
                    if ($mod > 0) {
                        $this->char['abilities'][$key]['disp'] = " (+" . $this->char['abilities'][$key]['mod'] . ")";
                    } elseif ($mod == 0) {
                        $this->char['abilities'][$key]['disp'] = "";
                    } else {
                        $this->char['abilities'][$key]['disp'] = " (" . $this->char['abilities'][$key]['mod'] . ")";
                    }
                }

                $this->char['abilities'][$key]['mod'] = $mod;
            }
        }
        protected function getLanguages()
        {
            $languages[] = 'Common';

            switch ($this->charClass) {
                case 'Elf':
                    $languages = [
                        'Common',
                        'Elf',
                        'Orc',
                        'Gnoll'
                    ];
                    break;
                case 'Dwarf':
                    $languages = [
                        'Common',
                        'Dwarf',
                        'Gnome',
                        'Goblin',
                        'Hobgoblin'
                    ];
            }

            $bonusLanguages = $this->char['abilities']['int']['mod'];
            while ($bonusLanguages > 0) {
                $possibleLanguages = $this->getLanguageList();
                shuffle($possibleLanguages);
                if (!in_array(($pop = array_pop($possibleLanguages)), $languages)) {
                    $languages[] = $pop;
                    $bonusLanguages--;
                }
            }

            return $languages;
        }

        protected function getNote()
        {
            $note[] = "Choose an alignment, and add alignment language to list of languages.";
            if ($this->charClass == 'Elf') {
                $note[] = 'Elfs add +2 to their Hit Die and HP total if they choose Fighter as their first class.';
            }
            return $note;
        }
        protected function generateArmor()
        {
            $this->char['AC'] = 9;
            if ($this->charClass != 'Magic-User') {
                $num = $this->util->roll('1d20');
                $armorDescriptor = (mt_rand(0, 1) === 0) ? $this->library->getRand('armorDescriptors') . " " : "";
                if ($num == 20 && $this->charClass == 'Fighter') {
                    $this->char['AC'] = 3;
                    $this->char['equipment'][] = $armorDescriptor . $this->library->getRand('heavyArmor');
                } elseif ($num >= 4 && $num <= 14) {
                    $this->char['AC'] = 7;
                    $this->char['equipment'][] = $armorDescriptor . $this->library->getRand('lightArmor');
                } elseif ($num >= 15 && $num <= 20) {
                    $this->char['AC'] = 5;
                    $this->char['equipment'][] = $armorDescriptor . $this->library->getRand('mediumArmor');
                }
                // shield and/or helm
                $num = $this->util->roll('1d20');
                $helmetDescriptor = (mt_rand(0, 1) === 0) ? $this->library->getRand('armorDescriptors') . " " : "";
                $shieldDescriptor = (mt_rand(0, 1) === 0) ? $this->library->getRand('armorDescriptors') . " " : "";
                if ($num >= 4 && $num <= 10) {
                    $this->char['equipment'][] = $helmetDescriptor . $this->library->getRand('helmet');
                } elseif ($num >= 11 && $num <= 17) {
                    $this->char['equipment'][] = $shieldDescriptor . $this->library->getRand('shield');
                    $this->char['AC']--;
                } elseif ($num >= 18 && $num <= 20) {
                    $this->char['equipment'][] = $helmetDescriptor . $this->library->getRand('helmet');
                    $this->char['equipment'][] = $shieldDescriptor . $this->library->getRand('shield');
                    $this->char['AC']--;
                }
            }
        }
        protected function addExtraEquipment()
        {
            if ($this->charClass == 'Magic-User' OR $this->charClass == 'Elf') {
                $this->char['equipment'][] = "spellbook (1st-level spells)";
            }
            $this->char['equipment'][] = "supplies (food & water), 1 day";
            $this->char['equipment'][] = "supplies (food & water), 1 day";
            $this->char['equipment'][] = $this->util->roll('3d6') . ' coins';
        }
    }
}
