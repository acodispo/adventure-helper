<?php

namespace App\Service\Generator\Character {

    use Doctrine\Inflector\InflectorFactory;
    use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

    class Base
    {
        // protected string $name;
        // protected array $abilities;
        protected $library;
        protected $game;
        private $router;

        public function __construct(UrlGeneratorInterface $router)
        {
            $this->router = $router;
            $this->util = new \App\Util();
        }

        public function generateCharacter()
        {
            $this->library = new \App\Service\Library();

            $this->char['game'] = $this->getGame();
            $this->char['license'] = $this->getLicense();

            $this->char['name'] = $this->library->humanName();

            $this->char['trait'] = $this->randomTrait();

            $this->char['spells'] = $this->getSpells();

            $this->char['note'] = $this->getNote();

            $this->char['abilities'] = $this->generateAbilityScores();

            $this->assignAbilityModifiers();

            $this->char['languages'] = $this->getLanguages();
            if (!empty($this->char['languages'])) $this->char['languagesOneLine'] = implode(", ", $this->char['languages']);

            $this->char['HD'] = $this->getHD();
            $this->char['hp'] = $this->util->roll($this->char['HD']) + $this->char['abilities']['con']['mod'];
            $this->char['hp'] = $this->char['hp'] > 0 ? $this->char['hp'] : 1;

            $bond = [
                "of" => $this->library->getRand('bondTypes'),
                "to" => $this->library->getRand('charTypes'),
            ];

            $inflector = InflectorFactory::create()->build();
            $to = new \ImLiam\IndefiniteArticle\IndefiniteArticle($bond['to']);
            $this->char['bond'] = $bond["of"] . $to->parse();
            $this->char['bg'] = "Raised by " . $inflector->pluralize($this->library->getRand('charTypes'));

            // generate equipment
            $weaponDescriptor = ($this->util->roll('1d2') == 1) ? $this->library->getRand('weaponDescriptors') . ' ' : "";
            $this->char['equipment'][] = $weaponDescriptor . $this->library->getRand('weapons');
            // armour
            $this->char['armor'] = $this->generateArmor();
            // light source
            $num = $this->util->roll('1d20');
            if ($num >= 1 && $num <= 3) {
                $this->char['equipment'][] = "candles, 5";
            } elseif ($num >= 4 && $num <= 13) {
                $this->char['equipment'][] = "torches, 5";
            } elseif ($num >= 14 && $num <= 16) {
                $this->char['equipment'][] = "torches, 5";
                $this->char['equipment'][] = "torches, 5";
            } elseif ($num >= 17 && $num <= 20) {
                $this->char['equipment'][] = "lantern";
                $this->char['equipment'][] = "lamp oil, 4 flasks";
            }
            // dungeoneering equipment
            $result = $this->util->roll('2d3');
            while ($result > 0) {
                $this->char['equipment'][] = $this->library->getRand('dungeoneeringEquipment');
                $result--;
            }

            $this->addExtraEquipment();

            $this->char['equipmentOneLine'] = implode('; ', $this->char['equipment']);
        }

        protected function getLicense()
        {
            return false;
        }

        protected function randomTrait()
        {
            return null;
        }

        protected function getHD()
        {
            return '1d6';
        }

        protected function generateAbilityScores()
        {
            $abilities = [
                'str', 'int', 'wis', 'dex', 'con', 'cha'
            ];

            foreach ($abilities as $ability) {
                $results[$ability]['score'] = $this->util->roll('3d6');
            }
            return $results;
        }
        protected function assignAbilityModifiers()
        {
            foreach ($this->char['abilities'] as $key => $ability) {
                if ($ability['score'] > 17) {
                    $this->char['abilities'][$key]['mod'] = 2;
                    $this->char['abilities'][$key]['disp'] = " (+" . $this->char['abilities'][$key]['mod'] . ")";
                } elseif ($ability['score'] > 14) {
                    $this->char['abilities'][$key]['mod'] = 1;
                    $this->char['abilities'][$key]['disp'] = " (+" . $this->char['abilities'][$key]['mod'] . ")";
                } elseif ($ability['score'] < 4) {
                    $this->char['abilities'][$key]['mod'] = -2;
                    $this->char['abilities'][$key]['disp'] = " (" . $this->char['abilities'][$key]['mod'] . ")";
                } elseif ($ability['score'] < 7) {
                    $this->char['abilities'][$key]['mod'] = -1;
                    $this->char['abilities'][$key]['disp'] = " (" . $this->char['abilities'][$key]['mod'] . ")";
                } else {
                    $this->char['abilities'][$key]['mod'] = 0;
                    $this->char['abilities'][$key]['disp'] = "";
                };
            }
        }

        protected function getGame()
        {
            return 'base';
        }

        protected function getSpells()
        {
            return false;
        }
        protected function getLanguages()
        {
            return false;
        }

        protected function getNote()
        {
            return false;
        }

        protected function getLanguageList()
        {
            return [
                'Bugbear', 'Doppleganger', 'Dragon', 'Dwarf', 'Elf', 'Gargoyle',
                'Gnoll', 'Gnome', 'Goblin', 'Harpy', 'Hobgoblin', 'Kobold',
                'Lizard Man', 'Medusa', 'Minotaur', 'Ogre', 'Orc', 'Pixie'
            ];
        }
        protected function generateArmor()
        {
            $this->char['AC'] = 9;
            $num = $this->util->roll('1d20');
            $armorDescriptor = (mt_rand(0, 1) === 0) ? $this->library->getRand('armorDescriptors') . " " : "";
            if ($num >= 4 && $num <= 14) {
                $this->char['equipment'][] = $armorDescriptor . $this->library->getRand('lightArmor') . " (light armour) *or* 1 spellbook";
                $this->char['AC'] = 7;
            } elseif ($num >= 15 && $num <= 20) {
                $this->char['equipment'][] = $armorDescriptor . $this->library->getRand('mediumArmor') . " (medium armour) *or* 2 spellbooks";
                $this->char['AC'] = 5;
            }
            // shield and/or helm
            $num = $this->util->roll('1d20');
            $helmetDescriptor = (mt_rand(0, 1) === 0) ? $this->library->getRand('armorDescriptors') . " " : "";
            $shieldDescriptor = (mt_rand(0, 1) === 0) ? $this->library->getRand('armorDescriptors') . " " : "";
            if ($num >= 4 && $num <= 10) {
                $this->char['equipment'][] = $helmetDescriptor . $this->library->getRand('helmet');
            } elseif ($num >= 11 && $num <= 17) {
                $this->char['equipment'][] = $shieldDescriptor . $this->library->getRand('shield');
                $this->char['AC']--;
            } elseif ($num >= 18 && $num <= 20) {
                $this->char['equipment'][] = $helmetDescriptor . $this->library->getRand('helmet');
                $this->char['equipment'][] = $shieldDescriptor . $this->library->getRand('shield');
                $this->char['AC']--;
            }
        }
        protected function addExtraEquipment()
        {
            $this->char['equipment'][] = "supplies (food & water), 1 day";
            $this->char['equipment'][] = "supplies (food & water), 1 day";
            $this->char['equipment'][] = $this->util->roll('3d6') . ' coins';
        }
    }
}
