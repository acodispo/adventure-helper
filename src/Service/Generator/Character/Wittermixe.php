<?php

namespace App\Service\Generator\Character {

    use App\Generator\Character;

    class Wittermixe extends Base
    {
        protected function randomTrait()
        {
            return $this->library->getRand('beginningTraits');
        }
        protected function getGame()
        {
            return 'wittermixe';
        }
    }
}
