<?php

namespace App\Service {
    class HazardRoller
    {
        protected $table;
        function rollHazard()
        {
            $this->util = new \App\Util();
            $this->roll = $this->util->roll('1d6');
            $this->body = "";
            if ($this->roll == 1) {
                $this->body .= '<p><strong>Encounter!</strong></p>';
                $this->body .= ( !empty($this->table) ? $this->util->parseTable($this->table) : "Roll on encounter table for type.");

                $double = $this->util->roll('1d6');
                if ($double == 1) {
                    $this->body .= '<p>Double encounter! We encounter the above encounting the below:</p>';
                    $this->body .= (!empty($this->table) ? $this->util->parseTable($this->table) : "Roll on encounter table for type.");
                }

                $surpriseParty = $this->util->roll('1d6');
                $surpriseMonster = $this->util->roll('1d6');

                if ($surpriseParty == 1 && $surpriseMonster != 1) {
                    $surprise = "Party surprised";
                } elseif ($surpriseParty != 1 && $surpriseMonster == 1) {
                    $surprise = "Monsters surprised";
                } elseif ($surpriseParty == 1 && $surpriseMonster == 1) {
                    $surprise = "Party surprised, Monsters surprised";
                } else {
                    $surprise = "No surprise";
                }

                if ($surpriseParty == 1 or $surpriseMonster == 1) {
                    $distance = $this->util->roll('1d6') * 10;
                } else {
                    $distance = $this->util->roll('2d6') * 10;
                }

                $this->body .= '<p><strong>Distance:</strong> ' . $distance . 'ft</p>';
                $this->body .= '<p><strong>Surprise:</strong> ' . $surprise . '</p>';
            } elseif ($this->roll == 2) {
                $this->body .= '<strong>Spoor:</strong> ' . ( !empty($this->table) ? $this->util->parseTable($this->table) : "Roll on encounter table for spoor type.");
            } elseif ($this->roll == 3) {
                $this->body .= "Expiration.";
            } elseif ($this->roll == 4) {
                $this->body .= "Rest or suffer 1hp.";
            } else {
                $this->body .= "No event.";
            }
        }

        public function getResult()
        {
            return (array) $this;
        }

        public function setTable(string $table = null)
        {
            $this->table = $table;
        }
    }
}
