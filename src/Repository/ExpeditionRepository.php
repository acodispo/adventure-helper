<?php

namespace App\Repository;

use App\Entity\Expedition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Expedition|null find($id, $lockMode = null, $lockVersion = null)
 * @method Expedition|null findOneBy(array $criteria, array $orderBy = null)
 * @method Expedition[]    findAll()
 * @method Expedition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExpeditionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Expedition::class);
    }

    public function findWithParty($id): object
    {
        $expedition = $this->find($id);

        $party = [];
        $primaryList = [];
        $retainerList = [];

        $conn = $this->getEntityManager()->getConnection();

        $sql = '
                SELECT pc.id, name, share, note
                FROM expedition_participants as sp
                JOIN pcs as pc on sp.pc_id = pc.id
                WHERE sp.expedition_id = ?
                ORDER BY share, name
                ';
        $stmt = $conn->prepare($sql);
        $stmt->execute([$expedition->getId()]);

        // returns an array of arrays (i.e. a raw data set)
        $party = $stmt->fetchAllAssociative();

        foreach ($party as $pc) {
            $pcDisplay = $pc['name'] . ' (' . $pc['note'] . ')';
            if ($pc['share'] == 1) {
                $primaryList[] = $pcDisplay;
            } else {
                $retainerList[] = $pcDisplay;
            }
        }

        $party['party'] = $party;
        $party['partyDisplay'] = implode(', ', $primaryList);
        if (!empty($retainerList)) {
            $party['partyDisplay'] .= '; ' . implode(', ', $retainerList);
        }

        $expedition->setParty($party);

        return $expedition;
    }

    public function findAllWithParties(): array
    {
        $expeditions = $this->findAll();

        foreach ($expeditions as $expedition) {
            $this->findWithParty($expedition->getId());
        }

        return $expeditions;
    }
}
