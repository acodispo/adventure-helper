<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Expedition;

class ExpeditionController extends AbstractController
{
    /**
     * @Route("/expedition", name="expedition")
     */
    public function index(): Response
    {
        $expeditions = $this->getDoctrine()
            ->getRepository(Expedition::class)
            ->findAllWithParties();

        return $this->render('expedition/index.html.twig', [
            'expeditions' => $expeditions,
        ]);
    }
    /**
     * @Route("/expedition/{id}", name="expedition_show")
     */
    public function show(int $id): Response
    {
        $expedition = $this->getDoctrine()
            ->getRepository(Expedition::class)
            ->findWithParty($id);

        if (!$expedition) {
            throw $this->createNotFoundException(
                'No expedition found for id ' . $id
            );
        }

        return $this->render('expedition/show.html.twig', ['expedition' => $expedition]);
    }
}
