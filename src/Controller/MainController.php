<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\Generator\Character;
use App\Service\HazardRoller;

class MainController extends AbstractController
{
    /**
     * @Route("/generate/character/{game}/{output}", name="generate_character")
     */
    // TODO: need a second route to handle the different outputs, i.e. game+output
    // and possibly a third route?
    public function generateCharacter(Character $character, string $game = 'base', string $output = 'base'): Response
    {
        $character->generate($_POST['game'] ?? $game);

        if (!empty($_POST['name']) OR !empty($_GET['name'])) {
            $character->name = $_POST['name'] ?? $_GET['name'];
        }

        return $this->render("generate/character.$output.html.twig", [
            'char' => $character,
            'url' => http_build_query($character),
            'output' => $output
        ]);
    }
    /**
     * @Route("/load/character/{output}", name="load_character")
     */
    public function loadCharacter(string $output = 'base'): Response
    {
        return $this->render("generate/character.$output.html.twig", [
            'char' => $_GET,
            'url' => http_build_query($_GET),
            'output' => $output
        ]);
    }
    /**
     * @Route("/hazard", name="hazard")
     */
    public function hazard(HazardRoller $hazard): Response
    {
        $hazard->setTable($_GET['table'] ?? null);
        $hazard->rollHazard();
        $result = $hazard->getResult();

        return $this->render("hazard.html.twig", [
            'table' => $_GET['table'] ?? null,
            'hazard' => $result
        ]);
    }
    /**
     * @Route("/ogl/", name="ogl")
     */
    public function ogl(): Response
    {
        $ogl = file_get_contents(__DIR__ . '/../../LICENSE-OpenGameLicense.markdown');
        return $this->render("base.markdown.twig", [
            'body' => $ogl
        ]);
    }
    /**
     * @Route("/", name="home")
     */
    public function homePage(): Response
    {
        return $this->render("home.markdown.twig", [
        ]);

    }
}
