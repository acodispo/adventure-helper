# Adventure Helper

A helper webapp for tabletop RPGs & adventure games.

Currently, a simple 0-level character generator for old-school games (circa 1974).

## Features

 * Creates a 0-level character.
 * Standard six ability scores from 3-18.
 * HP, modified by Con.
 * Random background information.
 * Equipment.
 * Markdown mode

## Usage

You can use the hosted version: <https://adventurehelper.wq64.net/> 

Or clone the repository & install your own! It is a [Symfony](https://symfony.com) app.